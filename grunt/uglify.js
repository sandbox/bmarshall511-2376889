module.exports = {
    dev: {
        options: {
          beautify: true,
          mangle: false,
          compress: false
        },
        files: {
            'build/js-dev/mailchimp-reports.js': ['src/js/mailchimp-reports.js'],
        }
    },
    prod: {
        files: {
            'build/js/mailchimp-reports.js': ['src/js/mailchimp-reports.js'],
        }
    }
};
