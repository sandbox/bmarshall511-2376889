Drupal MailChimp Reports module:
------------------------
Maintainers:
  Ben Marshall (https://www.drupal.org/u/bmarshall)
Requires - Drupal 7
License - GPL (see LICENSE)

Installation:
------------
1. Download and unpack the Libraries module directory in your modules folder
   (this will usually be "sites/all/modules/").
   Link: http://drupal.org/project/libraries
2. Download and unpack the MailChimp Reports module directory in your modules folder
   (this will usually be "sites/all/modules/").
3. Download and unpack the MailChimp Campaign CSV Parser library in "sites/all/libraries".
    Make sure the path to the library file becomes:
    "sites/all/libraries/mailchimp-campaign-csv-parser/lib/MailChimp_Campaign_CSV_Parser.class.php"
   Link: https://github.com/bmarshall511/mailchimp-campaign-csv-parser
4. Go to "Administer" -> "Modules" and enable the MailChimp Reports module.
